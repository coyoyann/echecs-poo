package view;

import shared.GUICoord;
import shared.PieceSquareColor;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;

/**
 * @author francoiseperrin
 * 
 * Classe d'affichage des carrés du damier
 * leur couleur est initialisé par les couleurs par défaut du jeu
 * puis peut être changé lorsque l'utilisateur 
 * sélectionne une nouvelle couleur à travers le menu
 *
 */
class SquareGui extends BorderPane implements ChessSquareGui {

	private PieceSquareColor squareColor;    	// le carré est Noir ou Blanc
	private GUICoord gUICoord;					// les coordonnées du carré sur le damier
	
	private static ObjectProperty<Color> lightColor = new SimpleObjectProperty<Color>();
	private static ObjectProperty<PaintStyle> paintStyle = new SimpleObjectProperty<PaintStyle>();
	private ObjectProperty<Color> backgroundColor = new SimpleObjectProperty<Color>();

	public SquareGui (GUICoord gUICoord, PieceSquareColor squareColor) {
		super();
		this.squareColor = squareColor;
		this.gUICoord = gUICoord;
		SquareGui.lightColor.bind(GuiFactory.lightColor);
		
		//Style (uni ou dégradé) est définie par les valeurs par défaut de configuration
		SquareGui.paintStyle.bind(GuiFactory.paintStyle);
		GuiFactory.paintStyleProperty().addListener(new ChangeListener<PaintStyle>() {
			  @Override
			  public void changed(ObservableValue<? extends PaintStyle> observable, PaintStyle oldValue, PaintStyle newValue) {
			    SquareGui.this.setBackgroundColor(SquareGui.this.backgroundColor.get());
			  }
			}); 
		
		// la couleur est définie par les valeurs par défaut de configuration
		if(PieceSquareColor.BLACK.equals(this.squareColor)) {
			this.backgroundColor.bind(GuiFactory.blackSquareColor);
		}
		else {
			this.backgroundColor.bind(GuiFactory.whiteSquareColor);
		}
		this.setBackgroundColor(this.backgroundColor.get());

		GuiFactory.blackSquareColorProperty().addListener(new ChangeListener<Color>() {
			  @Override
			  public void changed(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
			    SquareGui.this.setBackgroundColor(SquareGui.this.backgroundColor.get());
			  }
			});  
		
		GuiFactory.whiteSquareColorProperty().addListener(new ChangeListener<Color>() {
			  @Override
			  public void changed(ObservableValue<? extends Color> observable, Color oldValue, Color newValue) {
			    SquareGui.this.setBackgroundColor(SquareGui.this.backgroundColor.get());
			  }
			}); 
		

	}

	/**
	 * @return the coord
	 */
	public GUICoord getCoord() {
		return gUICoord;
	}

	/**
	 * @param isLight
	 * positionne la couleur en fonction du booléen
	 */
	public void resetColor(boolean isLight) {
		Color color = isLight ? SquareGui.lightColor.get() : this.backgroundColor.get();
		this.setBackgroundColor(color);
	}

	/**
	 * @param color
	 * setBackGround
	 */
	private void setBackgroundColor (Color color) {
		
		if(PaintStyle.GRADIENT.equals(SquareGui.paintStyle.get())) {
			Stop[] stops = new Stop[] { new Stop(0, color), new Stop(1, Color.WHITE)};
			LinearGradient lg1 = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
			this.setBackground(new Background(new BackgroundFill(lg1, CornerRadii.EMPTY, Insets.EMPTY)));	
		}
		else {
			this.setBackground(new Background(new BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY)));
		}
		this.setBorder(new Border(new BorderStroke(GuiFactory.blackSquareColor.get(), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

	}

}
