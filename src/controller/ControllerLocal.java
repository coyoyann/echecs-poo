package controller;

import model.ChessModel;
import shared.GUICoord;
import shared.ModelCoord;
import shared.PieceSquareColor;
import view.ChessView;

import java.util.List;
import java.util.stream.Collectors;

public class ControllerLocal extends AbstractController implements ChessControllerView, ChessControllerModel {

    private ChessModel chessModel;
    private ChessView chessView;
    private ModelCoord initialModelCoords;
    private GUICoord initialGuiCoords;
    private PieceSquareColor currentPlayenColor;

    public ControllerLocal() {
        super();
        this.currentPlayenColor = PieceSquareColor.WHITE; //Les blancs commencent
    }

    @Override
    public void setModel(ChessModel chessModel) {
        this.chessModel = chessModel;
    }

    @Override
    public void setView(ChessView chessGUI) {
        this.chessView = chessGUI;
    }

    @Override
    public boolean actionsWhenPieceIsSelectedOnGui(PieceSquareColor pieceSquareColor, GUICoord pieceToMoveCoord) {
        if (pieceSquareColor != currentPlayenColor){
            return false;
        }

        initialGuiCoords = pieceToMoveCoord;
        ModelCoord pieceToMoveModelCoord = CoordToModelCoord(pieceToMoveCoord);
        initialModelCoords = pieceToMoveModelCoord;

        List<ModelCoord> okMoves = this.chessModel.getPieceListMoveOK(pieceToMoveModelCoord);
        List<GUICoord> okMovesGui = this.ListModelCoordToListGUICoord(okMoves);
        this.chessView.resetLight(okMovesGui, true);
        return true;
    }

    @Override
    public boolean actionsWhenPieceIsDraggedOnGui(PieceSquareColor pieceSquareColor, GUICoord pieceToMoveCoord) {
        if (pieceSquareColor != this.currentPlayenColor) {
            return false;
        }
        this.chessView.setPieceToMoveVisible(initialGuiCoords, false);
        return true;
    }

    @Override
    public void actionsWhenPieceIsMovedOnGui(GUICoord targetCoord) {
        if (targetCoord != this.initialGuiCoords) {
            ModelCoord targetModelCoord = CoordToModelCoord(targetCoord);
            this.chessModel.move(this.initialModelCoords, targetModelCoord);
            this.chessView.movePiece(initialGuiCoords, targetCoord);
            this.chessView.setPieceToMoveVisible(targetCoord, true);
            this.currentPlayenColor = this.currentPlayenColor == PieceSquareColor.WHITE ? PieceSquareColor.BLACK : PieceSquareColor.WHITE; //Alternance blanc/noir
        } else {
            this.chessView.setPieceToMoveVisible(initialGuiCoords, true);
        }
    }

    @Override
    public void actionsWhenPieceIsReleasedOnGui(GUICoord targetCoord) {
        List<ModelCoord> okMoves = this.chessModel.getPieceListMoveOK(this.initialModelCoords);
        List<GUICoord> okMovesGui = this.ListModelCoordToListGUICoord(okMoves);
        this.chessView.resetLight(okMovesGui, false);
    }

    /**
     * Convertit une liste de ModelCoord en une liste de GUICoord
     * @param listModelCoord Une liste de ModelCoord
     * @return Une liste de GUICoord
     */
    private List<GUICoord> ListModelCoordToListGUICoord(List<ModelCoord> listModelCoord) {
        return listModelCoord.stream().map(this::ModelCoordToCoord).collect(Collectors.toList());
    }
}
