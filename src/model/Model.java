package model;

import shared.ActionType;
import shared.ModelCoord;

import java.util.ArrayList;
import java.util.List;

public class Model implements ChessModel {
    @Override
    public List<ModelCoord> getPieceListMoveOK(ModelCoord initCoord) {
        //Pour le moment, seule la case d'origine de la pièce sera illuminée
        List<ModelCoord> listMoveOK = new ArrayList<ModelCoord>();
        listMoveOK.add(initCoord);
        return listMoveOK;
    }

    @Override
    public ActionType move(ModelCoord initCoord, ModelCoord finalCoord) {
        return ActionType.MOVE;
    }

    @Override
    public boolean pawnPromotion(ModelCoord promotionCoord, String promotionType) {
        return false;
    }

    @Override
    public boolean isEnd() {
        return false;
    }
}
