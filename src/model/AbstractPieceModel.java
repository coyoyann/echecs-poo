package model;

import controller.AbstractController;
import shared.ActionType;
import shared.ModelCoord;
import shared.PieceSquareColor;

import java.util.List;

public abstract class AbstractPieceModel implements ChessPieceModel {

    public AbstractPieceModel() {

    }

    /**
     * Coordonnées de la pièce
     */
    private ModelCoord coord;

    /**
     * Représentation textuelle d'une pièce
     */
    public String toString() {
        return this.getName().substring(0, 2) + " " + this.getCoord();
    }

    @Override
    public ModelCoord getCoord() {
        return this.coord;
    }

    @Override
    public PieceSquareColor getCouleur() {
        return null;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public ActionType doMove(ModelCoord coord) {
        return null;
    }

    @Override
    public boolean catchPiece() {
        return false;
    }

    @Override
    public boolean undoLastMove() {
        return false;
    }

    @Override
    public boolean undoLastCatch() {
        return false;
    }
}
